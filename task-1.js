const getData = () => {
    console.log("Request data...")
    const promise = new Promise((resolve, reject) => {
      setTimeout(() => {
          console.log('Preparing data...')
            const data = {
          server: 'aws',
          port: 2000,
          status: 'pending'
        }
        resolve(data)
      }, 2000);
    })
    promise.then((data) => {
      data.status = 'success'
      if (data.status === 'success') {
        console.log('Data received - ', data)
      }
    }).catch(err => console.log("Rejected ", err))
    return promise
  }
  
  console.log(getData())