window.addEventListener("load", function () {
  const getAllCategories = async () => {
    const response = await fetch(
      `https://www.themealdb.com/api/json/v1/1/categories.php`
    );
    let data = await response.json();
    data = data.categories.splice(0, 5);
    let key;

    for (key of data) {
        let main = document.getElementById("main");
        main.style.display = 'flex';
        main.style.flexDirection = 'column';
        main.style.alignItems = 'center';
        let cardWrapper = document.createElement("div");
        cardWrapper.classList.add("card");
        cardWrapper.style.width = '800px'
        let mealImg = document.createElement("img");
        mealImg.classList.add("card-img-top");
        let cardBody = document.createElement("div");
        cardBody.classList.add("card-body");
        let cardTitle = document.createElement("h5");
        cardTitle.classList.add("card-title");
        let cardText = document.createElement("p");
        cardText.classList.add("card-text");
        main.append(cardWrapper);
        cardWrapper.prepend(mealImg);
        cardWrapper.append(cardBody);
        cardBody.prepend(cardTitle);
        cardBody.append(cardText);
        
        
        mealImg.setAttribute('src', key.strCategoryThumb)
        cardTitle.textContent = key.strCategory;
        cardText.textContent = key.strCategoryDescription;
    }
  };

  getAllCategories();
});


// const promise = fetch('https://www.themealdb.com/api/json/v1/1/categories.php').then(response => response.json()).then(data => console.log(data.categories))